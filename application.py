#encoding: utf-8

from flask import Flask, render_template, request
import requests
import config

app = Flask(__name__)

@app.route("/")
def homepage():
    return render_template('home.html')

@app.route("/captcha")
def captcha():
    return render_template(
        'librecaptcha_form.html',
        captcha_url=config.CAPTCHA_URL,
        public_key=config.PUBLIC_KEY,
    )

@app.route("/captcha_result", methods=['POST'])
def captcha_result():
    if request.method=='POST':
        # We got to test the captcha solution before using form's fields
        captcha_result = requests.post(
            "%sverifychallenge" % config.CAPTCHA_URL,
            data={
                "librecaptcha-answer": request.form.get("librecaptcha-answer", ""),
                "private-key": config.PRIVATE_KEY,
            }
        ).json()
        result = {
            "name": request.form.get("name"),
            "check": request.form.get("check"),
            "success": captcha_result["success"],
        }
        return render_template('librecaptcha_result.html', result=result)
    else:
        return redirect("/captcha")

@app.route("/recaptcha")
def recaptcha():
    return render_template(
        'recaptcha_form.html',
        captcha_url=config.CAPTCHA_URL,
        public_key=config.PUBLIC_KEY,
    )

@app.route("/recaptcha_result", methods=['POST'])
def recaptcha_result():
    if request.method=='POST':
        # We got to test the captcha solution before using form's fields
        captcha_result = requests.post(
            "%srecaptcha/api/siteverify" % config.CAPTCHA_URL,
            data={
                "response": request.form.get("g-recaptcha-response", ""),
                "secret": config.PRIVATE_KEY,
            }
        ).json()
        result = {
            "success": captcha_result["success"],
            "name": request.form.get("name"),
            "check": request.form.get("check"),
        }
        return render_template('recaptcha_result.html', result=result)
    else:
        return redirect("/recaptcha")

if __name__=='__main__':
    app.run(
        debug=config.DEBUG,
        port=config.PORT,
    )
